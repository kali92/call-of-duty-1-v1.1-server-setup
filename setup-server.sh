#!/bin/bash

# Linux noob proof server setup script in 2018 - LUL
# This script has been tested on Ubuntu 16.04, Ubuntu 14.04 and Debian 8.
# Use this script at your own risk!
# Contact Cato @ ts3.dvotx.org for premium support (:D)

# Poop everything out - debugging purpose
#set -x

# Check if the script is ran as root
if [[ $EUID -ne "0" ]]; then
   echo "Sorry, you need to run this script as root. Exiting..." 1>&2
   exit 1
fi

# Making my own life easier...
USER=cod
HOMEDIR=/opt/cod
BASEDIR=/opt/cod/serverfiles
SERVDIR=/opt/cod/myserver
ARCHIVE=serverfiles.tar.gz
URL="https://kebab.rocks/share/cod/server/gotowned-server/$ARCHIVE -q"
PUB_IP=$(wget http://ipecho.net/plain -O - -q);
VERSION_ID=$(cat /etc/os-release | grep "VERSION_ID")
CHECK_USER=$(getent passwd | grep "^$USER:" | sed s/':'/' '/g | awk '{print $1}')

# We have to check if the user cod already exists as we you can't run the script twice in a time.
if [[ $CHECK_USER ]]; then
   echo "The user $USER already exists. Exiting..."
   exit 1
fi

# Check kernel architecture
LINUX_ARCH=$(uname -i | awk '{print $1}' )
echo "Checking your linux kernel architecture..."
if [[ "$LINUX_ARCH" -ne "x86_64" ]]; then
    echo "You are not running on a x86_64 linux kernel architecture. Do you want to take the risk and continue running the script?"
    echo "CAUTION: some package installation might fail if you continue!"
    while [[ $CONTINUE != "y" && $CONTINUE != "n" ]]; do
      read -p "Continue ? [y/n]: " -e CONTINUE
    done
    if [[ "$CONTINUE" = "n" ]]; then
      echo "Good choice! Exiting..."
      exit 4
    fi
fi

if [[ ! -d "$BASEDIR" ]]; then
  echo "The directory $BASEDIR does not exist. Creating the directory..."
  mkdir -p "$BASEDIR"
fi

if [[ ! -d "$SERVDIR" ]]; then
  echo "The directory $SERVDIR does not exist. Creating the directory..."
  mkdir -p "$SERVDIR"
fi

# Checking linux distro
if [[ "$VERSION_ID" != 'VERSION_ID="16.04"' ]] && [[ "$VERSION_ID" != 'VERSION_ID="14.04"' ]] && [[ "$VERSION_ID" != 'VERSION_ID="8"' ]]; then
  echo "This script does not support your current Linux distro version. Exiting..."
  exit 1

# Install packages that we need based on distro version 
elif [[ "$VERSION_ID" = 'VERSION_ID="16.04"' ]] || [[ "$VERSION_ID" = 'VERSION_ID="14.04"' ]] || [[ "$VERSION_ID" = 'VERSION_ID=8"' ]]; then
   echo "Your version of Ubuntu/Debian is supported"
   echo "Installing 32-bit shared libraries and other stuff"
   dpkg --add-architecture i386
   apt-get -yqq update && apt-get -yqq install libc6:i386 zlib1g:i386 sudo wget screen
fi

echo "Creating a new user $USER"
# Using adduser instead of useradd based on man page useradd - On Debian, administrators should usually use adduser(8) instead
adduser --shell /bin/bash --home $HOMEDIR --system --group --disabled-password --quiet --gecos "" $USER
usermod -aG sudo $USER
echo "Created the new user and added to the sudoers list"

# Server installation starting here
echo "Downloading the server files on the background, we will extract it to $BASEDIR when finished downloading"
wget $URL -O $BASEDIR/$ARCHIVE && tar -xf $BASEDIR/$ARCHIVE -C $BASEDIR
echo "Download and extraction finished"

echo "Symlinking $BASEDIR to $SERVDIR"
ln -s $BASEDIR/* $SERVDIR

echo "#!/bin/bash
cd $SERVDIR
echo \"Starting Call of Duty 1 v1.1 Server\"
screen -A -h 1500 -m -d -S myserver bash -c \"LD_PRELOAD=./codextended.so ./cod_lnxded +set g_gametype sd +set net_ip $PUB_IP +set net_port 28960 +exec dedicated.cfg +set sv_maxclients 16 +set sv_pure 1 +map_rotate\"
sleep 1
echo \"Started Call of Duty 1 v1.1 Server\"" >> $SERVDIR/start.sh && chmod +x $SERVDIR/start.sh

echo "#!/bin/bash
echo \"Stopping Call of Duty 1 v1.1 Server\"
screen -S myserver -X quit
sleep 1
echo \"Stopped Call of Duty 1 v1.1 Server\"" >> $SERVDIR/stop.sh && chmod +x $SERVDIR/stop.sh
echo "Server installation finished."

echo "Changing the ownership of some directories and files"
chown -Rf $USER:$USER $HOMEDIR

echo "Starting up your first gameserver with the user $USER right now!"
su - $USER -c "$SERVDIR/start.sh"

echo "Cleaning up your system..."
apt-get -yqq autoremove
rm $BASEDIR/$ARCHIVE